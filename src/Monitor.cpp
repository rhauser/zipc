/*
 * Monitor.cpp
 *
 *  Created on: Apr 13, 2016
 *      Author: kolos
 */

#include <sstream>

#include <ipc/Monitor.hpp>
#include <ipc/Socket.hpp>

const char * prefix = "inproc://monitor.";

tdaq::ipc::Monitor::Monitor(Socket& socket) :
        m_monitor_socket(socket.getContext(), zmq::socket_type::pair)
{
    std::ostringstream out;
    out << prefix << (void*) socket;
    std::string address(out.str());
    int rc = zmq_socket_monitor((void*) socket, address.c_str(), ZMQ_EVENT_ALL);
    if (rc != 0) {
        throw zmq::error_t();
    }

    rc = zmq_connect((void*) m_monitor_socket, address.c_str());
    assert(rc == 0);
}

tdaq::ipc::Monitor::~Monitor()
{
    this->abort();
}

void tdaq::ipc::Monitor::update()
{
    while (true) {
        zmq::message_t request;
        int r = m_monitor_socket.recv(&request, ZMQ_NOBLOCK);
        if (!r) {
            break;
        }

        const char* data = request.data<const char>();
        zmq_event_t event;
        memcpy(&event.event, data, sizeof(uint16_t));
        data += sizeof(uint16_t);
        memcpy(&event.value, data, sizeof(int32_t));

        zmq::message_t addr;
        r = m_monitor_socket.recv(&addr, ZMQ_NOBLOCK);
        if (!r) {
            break;
        }
        const char* str = addr.data<const char>();
        std::string address(str, addr.size());

        switch (event.event) {
            case ZMQ_EVENT_CONNECTED:
                on_event_connected(event, address.c_str());
                break;
            case ZMQ_EVENT_CONNECT_DELAYED:
                on_event_connect_delayed(event, address.c_str());
                break;
            case ZMQ_EVENT_CONNECT_RETRIED:
                on_event_connect_retried(event, address.c_str());
                break;
            case ZMQ_EVENT_LISTENING:
                on_event_listening(event, address.c_str());
                break;
            case ZMQ_EVENT_BIND_FAILED:
                on_event_bind_failed(event, address.c_str());
                break;
            case ZMQ_EVENT_ACCEPTED:
                on_event_accepted(event, address.c_str());
                break;
            case ZMQ_EVENT_ACCEPT_FAILED:
                on_event_accept_failed(event, address.c_str());
                break;
            case ZMQ_EVENT_CLOSED:
                on_event_closed(event, address.c_str());
                break;
            case ZMQ_EVENT_CLOSE_FAILED:
                on_event_close_failed(event, address.c_str());
                break;
            case ZMQ_EVENT_DISCONNECTED:
                on_event_disconnected(event, address.c_str());
                break;
            default:
                on_event_unknown(event, address.c_str());
                break;
        }
    }
}
