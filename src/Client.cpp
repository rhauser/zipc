/*
 * Client.cpp
 *
 *  Created on: Apr 11, 2016
 *      Author: kolos
 */

#include <iostream>

#include <ipc/Client.hpp>

namespace
{
    int32_t readEnvironment(const char* name) {
        int32_t timeout = 10000;
        const char * env = getenv(name);
        if (env) {
            timeout = std::atoi(env);
        }
        return timeout;
    }

    int32_t getTimeout()
    {
        static int32_t timeout(readEnvironment("TDAQ_IPC_TIMEOUT"));
        return timeout;
    }
}

tdaq::ipc::Client::Client(::zmq::socket_type type, ::zmq::context_t& context) :
        m_socket(new Socket(type, context))
{
    ;
}

tdaq::ipc::Client::Client(::zmq::socket_type type, const Endpoint& endpoint,
        ::zmq::context_t& context) :
        m_socket(new Socket(type, context))
{
    connect(endpoint);
}

tdaq::ipc::Client::~Client()
{
    try {
        disconnect();
    }
    catch (...) {

    }

    delete m_socket;
}

void tdaq::ipc::Client::connect(const Endpoint& endpoint)
{
    try {
        m_socket->connect(endpoint.getAddress());
        m_endpoint = endpoint;
    }
    catch (zmq::error_t& e) {
        std::cerr << "can't connect to the " << endpoint.getAddress()
                << " endpoint: " << e.what() << std::endl;
    }
}

void tdaq::ipc::Client::disconnect()
{
    try {
        m_socket->disconnect(m_endpoint.getAddress());
    }
    catch (zmq::error_t& e) {
        std::cerr << "couldn't disconnect from the endpoint: " << e.what()
                << std::endl;
    }
}

void tdaq::ipc::Client::recv(zmq::message_t& m, int32_t milliseconds)
{
    while (true) {
        if (m_socket->isFailed()) {
            throw zmq::error_t();
        }

        if (m_socket->canRead(milliseconds)) {
            m_socket->recv(&m);
            std::clog << "received data: size = " << m.size() << std::endl;
            break;
        }
    }
}

void tdaq::ipc::Client::send(zmq::message_t& m, int32_t milliseconds)
{
    while (true) {
        if (m_socket->isFailed() && !recover()) {
            throw zmq::error_t();
        }

        if (m_socket->canWrite(milliseconds)) {
            m_socket->send(m);
            std::clog << "sent data: " << m.size() << std::endl;
            break;
        }
    }
}

bool tdaq::ipc::Client::recover()
{
    return false;
}

zmq::message_t tdaq::ipc::Client::makeRequest(zmq::message_t& m, std::chrono::milliseconds timeout)
{
    std::chrono::system_clock::time_point start = std::chrono::system_clock::now();
    send(m, timeout.count());

    std::chrono::system_clock::time_point end = std::chrono::system_clock::now();
    int32_t passed = std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();

    if (passed > timeout.count()) {
        throw zmq::error_t();
    }

    zmq::message_t r;
    recv(r, timeout.count()-passed);
    return r;
}

zmq::message_t tdaq::ipc::Client::makeRequest(zmq::message_t& m)
{
    return makeRequest(m, std::chrono::milliseconds(getTimeout()));
}
