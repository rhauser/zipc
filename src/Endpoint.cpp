/*
 * Endpoint.cpp
 *
 *  Created on: Apr 11, 2016
 *      Author: kolos
 */

#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <ifaddrs.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>

#include <map>
#include <iostream>
#include <regex>
#include <sstream>

#include <ipc/Endpoint.hpp>

using namespace tdaq::ipc;

namespace
{

    std::map<Transport, std::string> EnumToString = { { Transport::inproc,
            "inproc" }, { Transport::ipc, "ipc" }, { Transport::tcp, "tcp" }, {
            Transport::pgm, "pgm" }, };

    std::map<std::string, Transport> StringToEnum = { { "inproc",
            Transport::inproc }, { "ipc", Transport::ipc }, { "tcp",
            Transport::tcp }, { "pgm", Transport::pgm }, };

    std::string makeAddress(Transport const& transport,
            const std::string& interface, int port)
    {
        std::ostringstream out;
        out << EnumToString[transport] << "://" << interface;
        if (transport == Transport::tcp || transport == Transport::pgm) {
            out << ":" << port;
        }
        return out.str();
    }

    std::vector<std::string> getAllAddresses()
    {
        std::vector<std::string> addresses;

        int sock = socket(AF_INET, SOCK_STREAM, 0);

        int len = 100 * sizeof(struct ifreq);
        struct ifconf ifc;
        ifc.ifc_len = len;
        ifc.ifc_buf = (char*) malloc(len);
        if (ioctl(sock, SIOCGIFCONF, &ifc) < 0) {
            std::cerr
                    << "Unable to obtain the list of all interface addresses: "
                    << strerror(errno) << std::endl;
        }
        else {
            int total = ifc.ifc_len / sizeof(struct ifreq);
            struct ifreq* ifr = ifc.ifc_req;

            for (int i = 0; i < total; i++) {
                if (ifr[i].ifr_addr.sa_family == AF_INET) {
                    struct sockaddr_in* iaddr =
                            (struct sockaddr_in*) &ifr[i].ifr_addr;

                    if (iaddr->sin_addr.s_addr != 0 && iaddr->sin_family == AF_INET) {
                        char dest[80];
                        const char* addrstr = inet_ntop(AF_INET, &iaddr->sin_addr,
                                dest, sizeof(dest));
                        addresses.push_back(addrstr);
                    }
                }
            }
        }

        close(sock);
        free(ifc.ifc_buf);

        return addresses;
    }

    const std::string LoopbackAddress("127.0.0.1");

    std::string getFirstSuitableAddress(Transport const& transport, const std::string & pattern)
    {
        if (transport != Transport::tcp) {
            return pattern;
        }
        std::vector<std::string> aa = getAllAddresses();
        if (pattern.empty() || pattern == "*") {
            for (size_t i = 0; i < aa.size(); ++i) {
                if (aa[i] != LoopbackAddress) {
                    return aa[i];
                }
            }
        }
        return pattern;
    }
}

const std::string Endpoint::defaultInterface("*");
const Transport Endpoint::defaultTransport(Transport::tcp);

Endpoint::Endpoint(Transport const& transport,
        const std::string& interface, uint32_t port) :
        m_transport(transport),
        m_interface(getFirstSuitableAddress(m_transport, interface)),
        m_port(port),
        m_address(makeAddress(m_transport, m_interface, m_port))
{
}

Endpoint::Endpoint(const std::string& interface, uint32_t port) :
        m_transport(defaultTransport),
        m_interface(getFirstSuitableAddress(m_transport, interface)),
        m_port(port),
        m_address(makeAddress(m_transport, m_interface, m_port))
{
}

Endpoint::Endpoint(uint32_t port) :
        m_transport(defaultTransport),
        m_interface(getFirstSuitableAddress(m_transport, defaultInterface)),
        m_port(port),
        m_address(makeAddress(m_transport, m_interface, m_port))
{
}

Endpoint Endpoint::fromString(std::string const & address)
{
    std::regex regex("(tcp|inproc|ipc|pgm):\\/\\/([^:]+)(?::([0-9]+))?");
    std::smatch match;

    if (std::regex_match(address, match, regex)) {
        if (match.size() == 4) {
            return Endpoint(StringToEnum[match[1].str()], match[2].str(), stoul(match[3].str()));
        }
        if (match.size() == 3) {
            return Endpoint(StringToEnum[match[1].str()], match[2].str());
        }
    }

    throw zmq::error_t();
}

