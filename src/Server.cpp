/*
 * Server.cpp
 *
 *  Created on: Apr 11, 2016
 *      Author: kolos
 */

#include <functional>
#include <iterator>
#include <regex>
#include <iostream>

#include <ipc/Server.hpp>

using namespace tdaq::ipc;

tdaq::ipc::Server::Server(::zmq::socket_type type, RequestHandler const& handler, int threads, Endpoint const& endpoint) :
    m_context(new ::zmq::context_t()),
    m_router(new Socket(zmq::socket_type::router, *m_context)),
    m_proxy_address("inproc://workers_" + std::to_string(reinterpret_cast<uint64_t>(this)))
{
    m_workers.resize(threads);
    for (int i = 0; i < threads; ++i) {
        m_workers[i].reset(new std::thread(std::bind(&tdaq::ipc::Server::worker, this, type, handler)));
    }

    m_proxy.reset(new std::thread(std::bind(&tdaq::ipc::Server::proxy, this)));

    bind(endpoint);
}

tdaq::ipc::Server::~Server()
{
    unbind();

    // this will stop zmq::proxy and terminate the worker threads
    m_router.reset(0);
    m_context.reset(0);

    for (std::vector<Thread>::iterator it = m_workers.begin(); it != m_workers.end(); ++it) {
        (*it)->join();
    }
    m_proxy->join();
}

void tdaq::ipc::Server::proxy()
{
    ::zmq::socket_t dealer(*m_context, zmq::socket_type::dealer);
    dealer.bind(m_proxy_address);
    try {
        zmq::proxy((void*) *m_router, (void*) dealer, 0);
    } catch (::zmq::error_t& ) {
        std::cout << "Proxy terminated" << std::endl;
    }
}

void tdaq::ipc::Server::worker(zmq::socket_type type, RequestHandler const& handler)
{
    ::zmq::socket_t socket(*m_context, type);
    socket.connect(m_proxy_address);

    while (true) {
        try {
            //  Wait for the next request from the client
            zmq::message_t request;
            socket.recv(&request);

            zmq::message_t reply;
            try {
                reply = handler(request);
            } catch (...) {
                // TODO: User exception - serialize it and send back to the client
                continue;
            }

            //  Send reply back to the client if necessary
            if (reply.size()) {
                socket.send(reply);
            }
        }
        catch (zmq::error_t & e) {
            std::cerr << "This exception is expected to stop the worker: " << e.what() << std::endl;
            break;
        }
    }
}

void Server::bind(const Endpoint& endpoint)
{
    try {
        getSocket().bind(endpoint.getAddress());
    }
    catch (zmq::error_t& e) {
        std::cerr << "can't bind to the " << endpoint.getAddress()
                << " endpoint: " << e.what() << std::endl;
        return;
    }

    char address[1024];
    size_t size = sizeof(address);
    getSocket().getsockopt(ZMQ_LAST_ENDPOINT, &address, &size);
    m_endpoint = Endpoint::fromString(address);
}

void Server::unbind()
{
    try {
        getSocket().unbind(m_endpoint.getAddress());
    }
    catch (zmq::error_t& e) {
        std::cerr << "can't unbind from the " << m_endpoint.getAddress()
                << " endpoint: " << e.what() << std::endl;
    }
}
