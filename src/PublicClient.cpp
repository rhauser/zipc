/*
 * PublicClient.cpp
 *
 *  Created on: Apr 20, 2016
 *      Author: kolos
 */

#include <iostream>

#include <ipc/PublicClient.hpp>

tdaq::ipc::PublicClient::PublicClient(::zmq::socket_type type, ::zmq::context_t& context) :
    Client(type, context)
{
    ;
}

tdaq::ipc::PublicClient::PublicClient(::zmq::socket_type type,
        const Partition& partition, const std::string& name, ::zmq::context_t& context) :
        Client(type, context),
        m_partition(partition),
        m_name(name)
{
    connect(m_partition, m_name);
}

tdaq::ipc::PublicClient::~PublicClient()
{
}

void tdaq::ipc::PublicClient::connect(const Partition& partition,
        const std::string& name)
{
    Client::connect(partition.lookup(name));
    m_partition = partition;
    m_name = name;
}

bool tdaq::ipc::PublicClient::recover()
{
    try {
        Endpoint e = m_partition.lookup(m_name);
        if (e != m_endpoint) {
            ::zmq::context_t& c = m_socket->getContext();
            ::zmq::socket_type t = m_socket->getType();
            delete m_socket;
            m_socket = new Socket(t, c);
            Client::connect(e);
            std::clog << "recovery succeeded with the new endpoint: "
                    << e.getAddress() << std::endl;
            return true;
        } else {
            std::cerr << "recovery failed - endpoint is old" << std::endl;
        }
    }
    catch (zmq::error_t& e) {
        std::cerr << "recovery failed: " << e.what() << std::endl;
        return false;
    }
    std::clog << "recovery failed" << std::endl;
    return false;
}
