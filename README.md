0) setup the path to ZMQ library

> export LD_LIBRARY_PATH=/afs/cern.ch/user/k/kolos/public/zeromq/lib:$LD_LIBRARY_PATH

1) run the naming server

> zipc_server

Note the endpoint which is printed out by the server

2) run the test server

> export IPC_INIT_REF=<zipc_server endpoint>
> zipc_test_server

3) run the test client

> export IPC_INIT_REF=<zipc_server endpoint>
> zipc_test_client