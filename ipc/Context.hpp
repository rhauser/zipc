/*
 * Context.hpp
 *
 *  Created on: Apr 8, 2016
 *      Author: kolos
 */

#ifndef _IPC_CONTEXT_HPP_
#define _IPC_CONTEXT_HPP_

#include <zmq.hpp>

namespace tdaq
{
    namespace ipc
    {
        class Context
        {
        public:
            static ::zmq::context_t& globalInstance();

            const zmq::context_t& getContext() const
            {
                return m_context;
            }

            zmq::context_t& getContext()
            {
                return m_context;
            }

        protected:
            Context(::zmq::context_t& context) : m_context(context) {
            }

        protected:
            ::zmq::context_t& m_context;

        private:
            Context()= delete;
        };
    }
}

#endif /* _IPC_CONTEXT_HPP_ */
