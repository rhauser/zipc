/*
 * Monitor.hpp
 *
 *  Created on: Apr 13, 2016
 *      Author: kolos
 */

#ifndef _IPC_MONITOR_HPP_
#define _IPC_MONITOR_HPP_

#include <zmq.hpp>

namespace tdaq
{
    namespace ipc
    {
        class Socket;

        class Monitor: public zmq::monitor_t
        {
        protected:
            zmq::socket_t m_monitor_socket;

        public:
            Monitor(Socket& socket);

            ~Monitor();

            void update();

            const zmq::socket_t& getMonitorSocket() const
            {
                return m_monitor_socket;
            }

            zmq::socket_t& getMonitorSocket()
            {
                return m_monitor_socket;
            }
        };
    }
}

#endif /* _IPC_MONITOR_HPP_ */
