/*
 * main.cpp
 *
 *  Created on: Apr 12, 2016
 *      Author: kolos
 */

#include <unistd.h>
#include <iostream>
#include <sstream>
#include <vector>
#include <unordered_map>
#include <ipc/Server.hpp>

using namespace tdaq::ipc;

namespace
{
    std::vector<std::string> split(const std::string & s)
    {
        std::istringstream iss(s);

        return std::vector<std::string>( { std::istream_iterator<std::string> {
                iss }, std::istream_iterator<std::string> { } });
    }

    std::unordered_map<std::string, std::string> bindings;
}

zmq::message_t requestHandler(zmq::message_t& request)
{
    std::string s(request.data<const char>(), request.size());
    std::cout << "Received request: " << s << std::endl;

    std::vector<std::string> tokens = split(s);

    if (tokens.size() < 2) {
        return zmq::message_t();
    }

    if (tokens[0] == "publish") {
        bindings[tokens[1]] = tokens[2];

        zmq::message_t reply(2);
        memcpy(reply.data(), "ok", 2);
        return reply;
    }
    else if (tokens[0] == "withdraw") {
        std::unordered_map<std::string, std::string>::const_iterator it =
                bindings.find(tokens[1]);

        zmq::message_t reply(2);
        if (it != bindings.end()) {
            bindings.erase(it);
            memcpy(reply.data(), "ok", 2);
        }
        else {
            memcpy(reply.data(), "no", 2);
        }
        return reply;
    }
    else if (tokens[0] == "resolve") {
        std::unordered_map<std::string, std::string>::const_iterator it =
                bindings.find(tokens[1]);

        if (it != bindings.end()) {
            zmq::message_t reply(it->second.size());
            memcpy(reply.data(), it->second.c_str(), it->second.size());
            return reply;
        }
        else {
            zmq::message_t reply(2);
            memcpy(reply.data(), "no", 2);
            return reply;
        }
    }

    return zmq::message_t();
}

int main(int, char**)
{
    Server server(zmq::socket_type::rep, requestHandler, 5, Endpoint(12345));
    std::cout << "Socket is bound to the " << server.getEndpoint().getAddress()
            << " endpoint" << std::endl;

    while (true) {
        ::sleep(1);
    }

    return 0;
}

