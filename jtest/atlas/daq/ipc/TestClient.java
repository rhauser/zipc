package atlas.daq.ipc;

public class TestClient
{
    static void test(Client client) throws InterruptedException
    {
        //  Do N requests, waiting each time for a response
        for (int request_nbr = 0; request_nbr != 100; ++request_nbr) {
            String request = new String("x");

            System.out.println("sending request " + request_nbr + " ... ");
            client.send(request);

            System.out.println(" waiting for replay ... ");
            //  Get the reply.
            String reply = client.recv();
            System.out.println("got it: " + reply);

            Thread.sleep(3000);
        }
    }

    public static void main(String[] args) throws InterruptedException
    {
	Partition p = new Partition();
	PublicClient client = new PublicClient(SocketType.req, p, "test");

	System.out.println("Connected to " + client.getEndpoint().getAddress());

	while (true) {
	    try {
		test(client);
	    }
	    catch (Exception e) {
		System.out.println("Error: " + e.getMessage());
	        Thread.sleep(1000);
	    }
	}
    }
}
