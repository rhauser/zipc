package atlas.daq.ipc;

import org.zeromq.ZMQ;

public interface SocketEventListener
{
    void accepted(ZMQ.Event event);
    void acceptFailed(ZMQ.Event event);
    void bindFailed(ZMQ.Event event);
    void closed(ZMQ.Event event);
    void closeFailed(ZMQ.Event event);
    void connected(ZMQ.Event event);
    void connectDelayed(ZMQ.Event event);
    void connectRetried(ZMQ.Event event);
    void disconnected(ZMQ.Event event);
    void listening(ZMQ.Event event);
    void unknown(ZMQ.Event event);
}
