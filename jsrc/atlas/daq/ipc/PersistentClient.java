package atlas.daq.ipc;

public class PersistentClient extends Client
{
    public PersistentClient(SocketType type, Endpoint endpoint, Context context)
    {
	super(type, endpoint, context);
    }

    public PersistentClient(SocketType type, Context context)
    {
	super(type, context);
    }

    public PersistentClient(SocketType type, Endpoint endpoint)
    {
	super(type, endpoint);
    }

    boolean recover()
    {
        try {
            socket = new Socket(socket.getSocketType(), socket.getContext());
            connect(endpoint);
            System.out.println("recovery succeeded");
            return true;
        }
        catch (Exception e) {
            System.out.println("recovery failed: " + e.getMessage());
        }
        return false;
    }
}
