package atlas.daq.ipc;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;
import java.lang.reflect.Method;

public class Partition
{
    private static final Client namingService = init();
    private final String name;

    private static Client init() {
        try {
            return new PersistentClient(SocketType.req, Endpoint.fromString(java.lang.System.getenv("IPC_INIT_REF")));
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
            return null;
        }
    }
    
    public Partition()
    {
	name = new String();
    }

    public Partition(String name) {
	this.name = name;
    }

    // public void publish(Server server, String name)

    public void withdraw(String name) {
	String message = "withdraw " + name;

	String reply = namingService.makeRequest(message);

	if (reply.equals("no")) {
	    throw new RuntimeException("Object is not published");
	}
    }

    public Endpoint lookup(String name) {
	String message = "resolve " + name;

	String reply = namingService.makeRequest(message);

	if (!reply.equals("no")) {
	    return Endpoint.fromString(reply);
	}
	throw new RuntimeException("Object is not published");
    }

    public <T> T lookup(Class<T> clazz, String name, SocketType type) {
	PublicClient client = new PublicClient(type, this, name, Context.globalInstance());
	return build(clazz, client);
    }
    
    @SuppressWarnings("unchecked")
    private <T> T build(Class<T> clazz, Client client) {
	return (T) Proxy.newProxyInstance(
		clazz.getClassLoader(), new Class[] { clazz },
                new InvocationHandler () {		    
		    @Override
		    public Object invoke(Object proxy, Method method, Object[] args)  {
			return null; //client.makeRequest(message);
		    }
		});
    }

    public String getName()
    {
        return name;
    }

}
