package atlas.daq.ipc;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Endpoint
{
    private final Transport transport;
    private final String host;
    private final int port;
    private final String address;

    static final String defaultHost = "*";
    static final int defaultPort = 0;
    static final Transport defaultTransport = Transport.tcp;
    static final Pattern pattern = Pattern.compile("([a-z]+)://([0-9\\\\.]+):([0-9]+)");
    
    public static Endpoint fromString(String s)
    {
	try {
	    Matcher matcher = pattern.matcher(s);
	    matcher.find();

	    if (matcher.groupCount() == 3) {
		return new Endpoint(
			Transport.valueOf(matcher.group(1)), matcher.group(2), Integer.valueOf(matcher.group(3)));
	    }
	}
	catch (Exception e) {
	    throw new RuntimeException("Bad endpoint: " + e.getMessage());
	}
	throw new RuntimeException("Bad endpoint: " + s);
    }

    public Endpoint(Transport transport, String host, int port) {
	this.transport = transport;
	this.host = host;
	this.port = port;
	this.address = transport + "://" + host + ":" + port;
    }

    public Transport getTransport()
    {
        return transport;
    }

    public String getHost()
    {
        return host;
    }

    public int getPort()
    {
        return port;
    }

    public String getAddress()
    {
        return address;
    }

    @Override
    public int hashCode()
    {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((address == null) ? 0 : address.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj)
    {
	if (this == obj) {
	    return true;
	}
	if (obj == null || getClass() != obj.getClass()) {
	    return false;
	}
	Endpoint other = (Endpoint) obj;
	if (address == null) {
	    if (other.address != null) {
		return false;
	    }
	}
	else if (!address.equals(other.address)) {
	    return false;
	}
	return true;
    }
}
