package atlas.daq.ipc;

import org.zeromq.ZMQ;

public enum SocketType
{
    req(ZMQ.REQ),
    rep(ZMQ.REP),
    dealer(ZMQ.DEALER),
    router(ZMQ.ROUTER),
    pub(ZMQ.PUB),
    sub(ZMQ.SUB),
    xpub(ZMQ.XPUB),
    xsub(ZMQ.XSUB),
    push(ZMQ.PUSH),
    pull(ZMQ.PULL),
    pair(ZMQ.PAIR);
    
    private final int value;

    SocketType(int value)
    {
	this.value = value;
    }

    public int getValue()
    {
        return value;
    }
}
